package com.seabook.tankgame.client;

public interface Boundary {
	int MAX_WIDTH = 1000;
	int MAX_HEIGHT = 570;
}
