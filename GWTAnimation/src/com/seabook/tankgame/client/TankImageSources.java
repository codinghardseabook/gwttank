package com.seabook.tankgame.client;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

public interface TankImageSources extends ClientBundle {
	
	@Source("tank-up.png")
	ImageResource tankUp();
	
	@Source("tank-right.png")
	ImageResource tankRight();
	
	@Source("tank-left.png")
	ImageResource tankLeft();
	
	@Source("tank-down.png")
	ImageResource tankDown();
	
	@Source("tank-evil-up.png")
	ImageResource tankEvilUp();
			 
	@Source("tank-evil-right.png")
	ImageResource tankEvilRight();
	
	@Source("tank-evil-left.png")
	ImageResource tankEvilLeft();
	
	@Source("tank-evil-down.png")
	ImageResource tankEvilDown();
	
	@Source("bullet.gif")
	ImageResource bullet();
	
	@Source("on-fire.png")
	ImageResource onFireImage();
}
