package com.seabook.tankgame.client;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Vector;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;

public class Tank {
	private TankDirection tankDirection;
	private boolean isEvil;
	private boolean isHit;
	private int internalCount;
	private int x;
	private int y;
	
	private Widget tankImage;
	private Map<String, Image> badTankImages = new HashMap<String, Image>();
	
	private Vector<TankBullet> tankBullets = new Vector<TankBullet>();
	
	public static final int TANK_WIDTH = 40;
	public static final int TANK_HEIGHT = 40;
	
	public Tank() {
		initEvilTankImages();
	}
	
	public Tank(TankDirection td, boolean isEvil, int x, int y, Widget tankImg) {
		this.tankDirection = td;
		this.isEvil = isEvil;
		this.x = x;
		this.y = y;
		this.tankImage = tankImg;
		
		initEvilTankImages();
	}
	
	public void initEvilTankImages() {
		if (isEvil) {
			TankImageSources images = GWT.create(TankImageSources.class);
			badTankImages.put("right", new Image(images.tankEvilRight()));
			badTankImages.put("left", new Image(images.tankEvilLeft()));
			badTankImages.put("down", new Image(images.tankEvilDown()));
			badTankImages.put("up", new Image(images.tankEvilUp()));
			badTankImages.put("on-fire", new Image(images.onFireImage()));
		}
	}

	public TankDirection getTankDirection() {
		return tankDirection;
	}

	public void setTankDirection(TankDirection tankDirection) {
		this.tankDirection = tankDirection;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public Widget getTankImage() {
		return tankImage;
	}

	public void setTankImage(Widget tankImage) {
		this.tankImage = tankImage;
	}
	
	public void drawSelf(AbsolutePanel panel) {
		panel.remove(tankImage);
		panel.add(this.tankImage, this.x, this.y);
	}
	
	public void addBullet(TankBullet bullet) {
		tankBullets.add(bullet);
	}

	public Vector<TankBullet> getTankBullets() {
		return tankBullets;
	}

	public void setTankBullets(Vector<TankBullet> tankBullets) {
		this.tankBullets = tankBullets;
	}

	public boolean isEvil() {
		return isEvil;
	}

	public void setEvil(boolean isEvil) {
		this.isEvil = isEvil;
	}

	public int getInternalCount() {
		return internalCount;
	}

	public void setInternalCount(int internalCount) {
		this.internalCount = internalCount;
	}
	
	public boolean need2ChangeDirection() {
		Random random = new Random();
		int next = random.nextInt(8);
		if (next < 3) {
			next = 3;
		}
		
		if (internalCount >= next) {
			return true;
		}
		
		return false;
	}
	
	public boolean isTime2Shoot() {
		Random random = new Random();
		int next = random.nextInt(9);
		
		if (need2ChangeDirection()) {
			return false;
		}
		
		if (need2ChangeDirection4Boundaries()) {
			return false;
		}
		
		if (next < 1) {
			return true;
		}
		
		return false;
		
	}
	
	public boolean need2ChangeDirection4Boundaries() {
		if (x < 0) {
			x = 10;
			this.setTankDirection(TankDirection.RIGHT);
			this.setTankImage(badTankImages.get(TankDirection.RIGHT.toString()));
			return true;
		} else if (x > Boundary.MAX_WIDTH) {
			x = Boundary.MAX_WIDTH - 10;
			this.setTankDirection(TankDirection.LEFT);
			this.setTankImage(badTankImages.get(TankDirection.LEFT.toString()));
			return true;
		} else if (y < 0) {
			y = 10;
			this.setTankDirection(TankDirection.DOWN);
			this.setTankImage(badTankImages.get(TankDirection.DOWN.toString()));
			return true;
		} else if (y > Boundary.MAX_HEIGHT) {
			y = Boundary.MAX_HEIGHT - 10;
			this.setTankDirection(TankDirection.UP);
			this.setTankImage(badTankImages.get(TankDirection.UP.toString()));
		}
		
		return false;
	}

	public Map<String, Image> getBadTankImages() {
		return badTankImages;
	}

	public void setBadTankImages(Map<String, Image> badTankImages) {
		this.badTankImages = badTankImages;
	}
	
	public boolean isHit() {
		return isHit;
	}

	public void setHit(boolean isHit) {
		this.isHit = isHit;
	}

	public boolean isHitByBullet(TankBullet bullet) {
		Widget bulletWidget = bullet.getBulletImg();
		int left = bulletWidget.getAbsoluteLeft();
		int top = bulletWidget.getAbsoluteTop();
		
		float center_x = left - TankBullet.BULLET_WIDTH / 2;
		float center_y = top + TankBullet.BULLET_HEIGHT / 2;
		
		float tank_left = this.getX() -  TANK_WIDTH / 2;
		float tank_top = this.getY() - TANK_HEIGHT / 2 ;
		
		float tank_right = this.getX() +  TANK_WIDTH / 2;
		float tank_down = this.getX() +  TANK_WIDTH / 2;
		
		
		if (center_x > tank_left && center_x < tank_right && center_y > tank_top && center_y < tank_down) {
			return true;
		}
		
		return false;
	}
	
	public void onFire(AbsolutePanel panel) {
		panel.remove(tankImage);
		panel.add(badTankImages.get("on-fire"), this.x, this.y);
	}		
	
	public void resetFire(AbsolutePanel panel) {
		panel.remove(badTankImages.get("on-fire"));
	}
	
	
}
