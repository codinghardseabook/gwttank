package com.seabook.tankgame.client;

import java.util.Random;

public enum TankDirection {
	UP("up"), DOWN("down"), RIGHT("right"), LEFT("left");
	
	private String desc;
	
	private TankDirection(String desc) {
		this.desc = desc;
	}
	
	public static TankDirection getRandomTankDirection() {
		Random random = new Random(System.currentTimeMillis());
		
		int randomNum = random.nextInt(4);
		
		switch (randomNum) {
		case 0:
			return UP;
		case 1:
			return DOWN;
		case 2:
			return RIGHT;
		default :
			return LEFT;
		}
	}
	
	@Override
	public String toString() {
		return desc;
	}
}