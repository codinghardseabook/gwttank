package com.seabook.tankgame.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;

public class TankBullet {
	private Widget bulletImg;

	private int x;
	private int y;

	public static final int BULLET_WIDTH;
	public static final int BULLET_HEIGHT;

	private TankDirection bulletDirection;

	private static TankImageSources images = GWT.create(TankImageSources.class);

	static {
		Image defaultImage = new Image(images.bullet());
		BULLET_WIDTH = defaultImage.getOffsetWidth();
		BULLET_HEIGHT = defaultImage.getOffsetHeight();
	}

	public TankBullet() {
		this.bulletImg = new Image(images.bullet());
	}

	public TankBullet(Widget bulletImg, int x, int y,
			TankDirection bulletDirection) {
		this.bulletImg = bulletImg;
		this.x = x;
		this.y = y;
		this.bulletDirection = bulletDirection;
	}

	public Widget getBulletImg() {
		return bulletImg;
	}

	public void setBulletImg(Widget bulletImg) {
		this.bulletImg = bulletImg;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public TankDirection getBulletDirection() {
		return bulletDirection;
	}

	public void setBulletDirection(TankDirection bulletDirection) {
		this.bulletDirection = bulletDirection;
	}

	public void drawSelf(AbsolutePanel panel) {
		panel.add(this.bulletImg, this.x, this.y);
	}

	public boolean isBulletOutOfBound() {
		if (x > Boundary.MAX_WIDTH || y > Boundary.MAX_HEIGHT || x < 0 || y < 0) {
			return true;
		}

		return false;
	}

	public boolean hitTank(Tank tank) {

		float center_x = this.x;
		float center_y = this.y;
		
		float tank_left = tank.getX() - Tank.TANK_WIDTH / 2;
		float tank_top = tank.getY() - Tank.TANK_HEIGHT / 2;
		
		float tank_right = tank.getX() + Tank.TANK_WIDTH / 2;
		float tank_down = tank.getY() + Tank.TANK_WIDTH / 2;

		if (center_x > tank_left && center_x < tank_right
				&& center_y > tank_top && center_y < tank_down) {
			return true;
		}

		return false;
	}
}
